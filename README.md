# Sisop Praktikum Modul 4 2023 MHFD IT10


# Soal 1

 __Analisa Soal__


Unduh dataset pemain sepak bola dari Kaggle dengan perintah kaggle datasets download -d bryanb/fifa-player-stats-database dan ekstrak file tersebut di dalam file storage.c.Baca file CSV FIFA23_official_data.csv di dalam storage.c dan cetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub selain Manchester City.Buat Dockerfile yang berisi langkah-langkah setup environment dan menjalankan aplikasi.Gunakan Docker CLI untuk membangun Docker Image dari Dockerfile.Publikasikan Docker Image ke Docker Hub dengan mengunggahnya ke https://hub.docker.com/r/{Username}/storage-app.Untuk skalabilitas, gunakan Docker Compose dengan 5 instance di folder terpisah "Barcelona" dan "Napoli" untuk mengelola permintaan dari klub sepak bola lain.


__Kode Program__

1. storage.c

```
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
```
stdlib.h diperlukan untuk menggunakan fungsi system() dan exit(), stdio.h diperlukan untuk fungsi input-output standar seperti printf() dan fopen(), serta string.h diperlukan untuk fungsi pemrosesan string seperti strtok() dan strcmp()

```
#define maksimal 3000
```
maksimal didefinisikan dengan nilai 3000. digunakan untuk menentukan ukuran maksimal array line pada baris selanjutnya.

```
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
system("unzip fifa-player-stats-database.zip");
```

fungsi system() untuk menjalankan perintah shell. Pertama, program akan mendownload dataset dengan perintah kaggle datasets download -d bryanb/fifa-player-stats-database. Kemudian, program akan mengekstrak file ZIP yang diunduh menggunakan perintah unzip fifa-player-stats-database.zip

```
FILE *progam_file;
char namafile[] = "FIFA23_official_data.csv";
progam_file = fopen(namafile, "r");
if (progam_file == NULL) {
    printf("Tidak bisa membuka file %s\n", namafile);
    exit(1);
}
```
program membuka file CSV dengan menggunakan fungsi fopen(). Nama file yang dibuka disimpan dalam array karakter namafile. Mode "r" digunakan untuk membuka file dalam mode baca. Jika file tidak dapat dibuka (misalnya, file tidak ada), program akan mencetak pesan kesalahan dan keluar dari program menggunakan fungsi exit().

```
char line[maksimal];
fgets(line, maksimal, progam_file);
```
menggunakan fungsi fgets() untuk membaca baris pertama dari file CSV, yang berisi header kolom. Baris ini akan dibaca dan disimpan dalam array karakter line

```
while (fgets(line, maksimal, progam_file)) {
    // ...
}
```
menggunakan perulangan while untuk membaca setiap baris dari file CSV setelah baris header. Setiap baris akan disimpan dalam array karakter line.

```
char *id = strtok(line, ",");
char *name = strtok(NULL, ",");
char *age = strtok(NULL, ",");
char *photo = strtok(NULL, ",");
char *nationality = strtok(NULL, ",");
char *flag = strtok(NULL, ",");
char *overall = strtok(NULL, ",");
char *potential = strtok(NULL, ",");
char *club = strtok(NULL, ",");
char *club_logo = strtok(NULL, ",");
char *value = strtok(NULL, ",");
char *wage = strtok(NULL, ",");
char *special = strtok(NULL, ",");
char *preferred_foot = strtok(NULL, ",");
char *international_reputation = strtok(NULL, ",");
char *weak_foot = strtok(NULL, ",");
char *skill_moves = strtok(NULL, ",");
char *work_rate = strtok(NULL, ",");
char *body_type = strtok(NULL, ",");
char *real_face = strtok(NULL, ",");
char *position = strtok(NULL, ",");
char *joined = strtok(NULL, ",");
char *loaned_from = strtok(NULL, ",");
char *contract_valid_until = strtok(NULL, ",");
char *height = strtok(NULL, ",");
char *weight = strtok(NULL, ",");
char *release_clause = strtok(NULL, ",");
char *kit_number = strtok(NULL, ",");
char *best_overall_rating = strtok(NULL, ",");
```
strtok(). Elemen data tersebut akan disimpan dalam variabel yang sesuai.Pemrosesan data jadi data yang ada diinialisasi agar dapat terbaca

```
strtok(). Elemen data tersebut akan disimpan dalam variabel yang sesuai.Pemrosesan data jadi data yang ada diinialisasi agar dapat terbaca
```
program melakukan pengecekan kriteria pada pemain yang saat ini sedang diproses. Jika pemain memenuhi kriteria (umur kurang dari 25 tahun, potensi lebih dari 85, dan bukan pemain dari klub "Manchester City"), maka pemain tersebut akan dicetak menggunakan fungsi printf()

2. Dockerfile
```
FROM ubuntu:20.04
RUN apt-get update && apt-get install -y \
    gcc \
    make \
    wget \
    unzip \
    libssl-dev \
    libcurl4-openssl-dev \
    python3-venv

WORKDIR /app

RUN apt-get install -y python3-pip && pip3 install kaggle
COPY kaggle.json /root/.kaggle/kaggle.json
RUN chmod 600 /root/.kaggle/kaggle.json

RUN python3 -m venv /venv
ENV PATH="/venv/bin:$PATH"

ENV KAGGLE_USERNAME=teresiaelvara
ENV KAGGLE_KEY=0bad854609573d980c80810e214ab3e2

CMD ["./storage"]

COPY storage.c /app/storage.c
RUN gcc -o storage storage.c

```
Penjelasan kode 
1. **FROM ubuntu:20.04**
Mendefinisikan base image yang digunakan untuk membangun container. Dalam hal ini, menggunakan Ubuntu versi 20.04.

2. **RUN apt-get update && apt-get install -y \ gcc \ make \ wget \ unzip \ libssl-dev \ libcurl4-openssl-dev \ python3-venv**
Melakukan pembaruan repository dan menginstal dependensi yang diperlukan. Di sini, gcc, make, wget, unzip, libssl-dev, libcurl4-openssl-dev, dan python3-venv diinstal dengan menggunakan perintah apt-get.

3. **WORKDIR /app**
Mengatur working directory di dalam container menjadi "/app".

4. **RUN apt-get install -y python3-pip && pip3 install kaggle**
Menginstal Python 3 dan pip menggunakan apt-get, dan menginstal paket kaggle melalui pip.

5. **COPY kaggle.json /root/.kaggle/kaggle.json**
Menyalin file "kaggle.json" dari direktori lokal ke direktori "/root/.kaggle/kaggle.json" di dalam container. File ini diperlukan untuk otentikasi dengan Kaggle API.

6. **RUN chmod 600 /root/.kaggle/kaggle.json**
Mengatur izin (permission) file "kaggle.json" menjadi 600 di dalam container.

7. **RUN python3 -m venv /venv**
Membuat virtual environment dengan menggunakan modul venv di Python 3 di direktori "/venv" di dalam container.

8. **ENV PATH="/venv/bin:$PATH"**
Mengatur environment variable PATH untuk mencakup direktori "/venv/bin" sehingga executable dalam virtual environment dapat diakses.

9. **ENV KAGGLE_USERNAME=teresiaelvara**
Mengatur environment variable KAGGLE_USERNAME dengan nilai "teresiaelvara". Variabel ini diperlukan untuk otentikasi dengan Kaggle API.

10. **ENV KAGGLE_KEY=0bad854609573d980c80810e214ab3e2**
Mengatur environment variable KAGGLE_KEY dengan nilai kunci API Kaggle. Nilai ini diperlukan untuk otentikasi dengan Kaggle API.

11. **EXPOSE 8080**
Menentukan port 8080 di container yang akan di-expose saat container berjalan.

12. **CMD ["./storage"]**
Menjalankan perintah "./storage" saat container dijalankan.

13. **COPY storage.c /app/storage.c**
Menyalin file "storage.c" dari direktori lokal ke dalam direktori "/app" di dalam container.

14. **RUN gcc -o storage storage.c**
Mengompilasi file "storage.c" menjadi executable bernama "storage" di dalam container.

3. Docker-Compose
```
services:
  instance1:
    image: elvaraelvara/storage-app
    ports:
      - 8181:80

  instance2:
    image: elvaraelvara/storage-app
    ports:
      - 8182:80

  instance3:
    image: elvaraelvara/storage-app
    ports:
      - 8183:80

  instance4:
    image: elvaraelvara/storage-app
    ports:
      - 8184:80

  instance5:
    image: elvaraelvara/storage-app
    ports:
      - 8185:80
```
Penjelasan kode
1. **version: '3'**
Menentukan versi dari docker-compose yang digunakan. Dalam hal ini, menggunakan versi 3.

2. **services:**
Mendefinisikan layanan-layanan (instances) yang akan dijalankan menggunakan Docker.

3. **instance1, instance2, instance3, instance4, instance5:**
Nama-nama layanan (instances) yang akan dijalankan. Dalam contoh ini, ada 5 layanan dengan nama instance1, instance2, instance3, instance4, dan instance5.



__Testing Output__

storage.c

![Teks alternatif](https://i.ibb.co/HtM5b0C/Screenshot-2023-06-03-140800.png)

Dockerfile

![Teks alternatif](https://i.ibb.co/0qTF8xN/Screenshot-2023-06-03-140938.png)

Docker Hub 

![Teks alternatif](https://i.ibb.co/7JYK4PD/Screenshot-2023-06-03-141225.png)

Docker Compose 
![Teks alternatif](https://i.ibb.co/RS26Lrg/Screenshot-2023-06-03-141125.png)

__Kendala__

Perlu menginstall banyak hal yang membuat storage penuh

# Soal 4
  __Analisa Soal__  
    Diminta membuat fuse, dimana di fuse tersebut semua system call dicatat ke log, dan memodularkan file di folder dengan awalan modul. kemudian jika folder di buka di fuse maka terlihat utuh, namun di asli akan terlihat modular.

  __Kode Program__  
    modular.c
    
    #define FUSE_USE_VERSION 28
    #include <fuse.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <fcntl.h>
    #include <dirent.h>
    #include <errno.h>
    #include <sys/time.h>
    #include <sys/stat.h>
    #include <limits.h>
    #include <time.h>

    #define CHUNK_SIZE 1024

    static FILE *log_file;

    static void write_to_log(const char *level, const char *operation, const char *path) {
        time_t rawtime;
        struct tm *timeinfo;
        char timestamp[20];

        time(&rawtime);
        timeinfo = localtime(&rawtime);
        strftime(timestamp, sizeof(timestamp), "%d%m%y-%H:%M:%S", timeinfo);

        fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, operation, path);
        fflush(log_file);
    }

    int is_chunk(const char* filename) {
        int len = strlen(filename);
        if (len > 4) {
            const char* extension = filename + len - 4;
            if (strcmp(extension, ".000") == 0) {
                return 1;
            }
        }
        return 0;
    }

    int has_modular_chunk(const char* filename) {
        int len = strlen(filename);
        if (len > 4) {
            const char* extension = filename + len - 4;
            if (strcmp(extension, ".000") == 0) {
                char* file_prefix = malloc(len - 7);  // Ekstensi ".000" diabaikan
                strncpy(file_prefix, filename, len - 7);
                file_prefix[len - 7] = '\0';

                DIR* dir = opendir(".");
                struct dirent* entry;
                while ((entry = readdir(dir)) != NULL) {
                    if (strncmp(entry->d_name, file_prefix, len - 7) == 0) {
                        free(file_prefix);
                        closedir(dir);
                        return 1;
                    }
                }

                free(file_prefix);
                closedir(dir);
            }
        }
        return 0;
    }

    void modularize_file(const char* file_path) {
        if (is_chunk(file_path) || has_modular_chunk(file_path)) {
            return;  // Jika file sudah dimodular atau memiliki chunk modular, lewati pemrosesan
        }

        // Dapatkan nomor urutan chunk dari file_path
        int chunk_index = 0;
        const char* extension = strrchr(file_path, '.');
        if (extension != NULL) {
            sscanf(extension, ".%03d", &chunk_index);
        }

        if (chunk_index > 0) {
            return;  // Jika file memiliki nomor urutan chunk yang lebih besar dari 0, lewati pemrosesan
        }

        FILE* file = fopen(file_path, "rb");
        if (file == NULL) {
            printf("Error opening file: %s\n", file_path);
            return;
        }

        unsigned char buffer[CHUNK_SIZE];
        size_t bytes_read;

        chunk_index = 0;
        char chunk_path[PATH_MAX];

        while ((bytes_read = fread(buffer, 1, CHUNK_SIZE, file)) > 0) {
            snprintf(chunk_path, sizeof(chunk_path), "%s.%03d", file_path, chunk_index);
            FILE* chunk_file = fopen(chunk_path, "wb");
            if (chunk_file == NULL) {
                printf("Error creating chunk file: %s\n", chunk_path);
                fclose(file);
                return;
            }
            fwrite(buffer, 1, bytes_read, chunk_file);
            fclose(chunk_file);
            chunk_index++;
        }

        fclose(file);
        remove(file_path);  // Menghapus file asli setelah pemisahan
    }

    void modularize_directory(const char* path) {
        DIR* dir = opendir(path);
        if (dir == NULL) {
            printf("Error opening directory: %s\n", path);
            return;
        }

        struct dirent* entry;
        char full_path[PATH_MAX];

        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }

            snprintf(full_path, sizeof(full_path), "%s/%s", path, entry->d_name);

            struct stat st;
            if (lstat(full_path, &st) == 0 && S_ISDIR(st.st_mode)) {
                modularize_directory(full_path);  // Rekursif untuk direktori yang ditemukan
            } else {
                if (!is_chunk(entry->d_name) && !has_modular_chunk(entry->d_name)) {
                    modularize_file(full_path);  // Memodularisasi file
                }
            }
        }

        closedir(dir);
    }

    void merge_files(const char* path) {
        DIR* dir = opendir(path);
        if (dir == NULL) {
            printf("Error opening directory: %s\n", path);
            return;
        }

        struct dirent* entry;
        char full_path[PATH_MAX];

        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }

            snprintf(full_path, sizeof(full_path), "%s/%s", path, entry->d_name);

            struct stat st;
            if (lstat(full_path, &st) == 0 && S_ISDIR(st.st_mode)) {
                merge_files(full_path);  // Rekursif untuk direktori yang ditemukan
            } else {
                char* extension = strrchr(entry->d_name, '.');
                if (extension != NULL && strcmp(extension, ".000") == 0) {
                    int chunk_index = 0;
                    char original_file[PATH_MAX];
                    strncpy(original_file, entry->d_name, extension - entry->d_name);
                    original_file[extension - entry->d_name] = '\0';

                    snprintf(full_path, sizeof(full_path), "%s/%s", path, original_file);
                    FILE* output_file = fopen(full_path, "wb");
                    if (output_file == NULL) {
                        continue;
                    }

                    do {
                        snprintf(full_path, sizeof(full_path), "%s/%s.%03d", path, original_file, chunk_index);
                        FILE* chunk_file = fopen(full_path, "rb");
                        if (chunk_file == NULL) {
                            break;
                        }

                        unsigned char buffer[CHUNK_SIZE];
                        size_t bytes_read;

                        while ((bytes_read = fread(buffer, 1, CHUNK_SIZE, chunk_file)) > 0) {
                            fwrite(buffer, 1, bytes_read, output_file);
                        }

                        fclose(chunk_file);
                        chunk_index++;
                    } while (1);

                    fclose(output_file);

                    // Menghapus file chunk
                    chunk_index = 0;
                    do {
                        snprintf(full_path, sizeof(full_path), "%s/%s.%03d", path, original_file, chunk_index);
                        if (unlink(full_path) == 0) {
                        }
                        chunk_index++;
                    } while (chunk_index <= 999);

                    unlink(original_file);
                }
            }
        }

        closedir(dir);
    }

    int isModule(const char *path) {
        const char *prefix = "module_";
        const char *filename = strrchr(path, '/');
        if (filename != NULL) {
            if (strncmp(filename + 1, prefix, strlen(prefix)) == 0) {
                return 1;
            }
        }
        return 0;
    }

    int isDirectory(const char *path) {
        struct stat pathStat;
        if (stat(path, &pathStat) == -1) {
            // Gagal mendapatkan informasi stat
            return 0;
        }

        return S_ISDIR(pathStat.st_mode);
    }

    int isFile(const char *path) {
        struct stat pathStat;
        if (stat(path, &pathStat) == -1) {
            // Gagal mendapatkan informasi stat
            return 0;
        }

        return S_ISREG(pathStat.st_mode);
    }

    void add_module_directory(const char *path) {
        FILE* file = fopen("module_directories.txt", "a");
        if (file != NULL) {
            if (fprintf(file, "%s\n", path) < 0) {
                perror("Gagal menulis ke file");
                fclose(file);
                exit(EXIT_FAILURE);
            }
            if (fclose(file) != 0) {
                perror("Gagal menutup file");
                exit(EXIT_FAILURE);
            }
        } else {
            perror("Gagal membuka file");
            exit(EXIT_FAILURE);
        }
    }

    void delete_module_directory(const char *path) {
        const char* file_path = "module_directories.txt";
        const char* temp_file_path = "temp_module_directories.txt";

        FILE* file = fopen(file_path, "r");
        if (file == NULL) {
            perror("Gagal membuka file");
            exit(EXIT_FAILURE);
        }

        FILE* temp_file = fopen(temp_file_path, "w");
        if (temp_file == NULL) {
            perror("Gagal membuat file sementara");
            fclose(file);
            exit(EXIT_FAILURE);
        }

        char line[256];
        int found = 0;
        size_t path_len = strlen(path);

        while (fgets(line, sizeof(line), file) != NULL) {
            if (strncmp(line, path, path_len) != 0) {
                fputs(line, temp_file);
            } else {
                found = 1;
            }
        }

        fclose(file);
        fclose(temp_file);

        if (remove(file_path) != 0) {
            perror("Gagal menghapus file");
            exit(EXIT_FAILURE);
        }

        if (rename(temp_file_path, file_path) != 0) {
            perror("Gagal mengganti nama file");
            exit(EXIT_FAILURE);
        }

    }

    char* findMatchingPath(const char* path) {
        FILE* file = fopen("module_directories.txt", "r");
        if (file == NULL) {
            printf("Gagal membuka file 'module_directories.txt'.\n");
            return NULL;
        }

        char line[512];
        while (fgets(line, sizeof(line), file)) {
            line[strcspn(line, "\n")] = '\0'; // Menghapus karakter newline di akhir baris

            if (strstr(path, line) != NULL) {
                fclose(file);
                char* matchingPath = malloc(strlen(line) + 1);
                strcpy(matchingPath, line);
                return matchingPath; // Path ditemukan dalam file
            }
        }

        fclose(file);
        return NULL; // Path tidak ditemukan dalam file
    }

    static int xmp_getattr(const char *path, struct stat *stbuf) {
        int res;
        res = lstat(path, stbuf);

        if (res == -1) return -errno;
        return 0;
    }

    static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
        DIR *dp;
        struct dirent *de;
        (void) offset;
        (void) fi;

        dp = opendir(path);

        if (dp == NULL) return -errno;

        while ((de = readdir(dp)) != NULL) {
            struct stat st;

            memset(&st, 0, sizeof(st));

            st.st_ino = de->d_ino;
            st.st_mode = de->d_type << 12;

            if(filler(buf, de->d_name, &st, 0)) break;
        }
        closedir(dp);
        return 0;
    }

    static int xmp_access(const char *path, int mask)
    {
        int res = access(path, mask);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "ACCESS", path);
        return 0;
    }

    static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
        int fd;
        int res;
        (void) fi;

        fd = open(path, O_RDONLY);

        if (fd == -1) return -errno;

        res = pread(fd, buf, size, offset);

        if (res == -1) res = -errno;

        close(fd);

        return res;
    }

    static int xmp_mkdir(const char *path, mode_t mode) {
        if (isModule(path)) add_module_directory(path);

        int res = mkdir(path, mode);
        if (res == -1)
            return -errno;

        // Cek apakah direktori memiliki awalan "module_"

        write_to_log("REPORT", "MKDIR", path);

        return 0;
    }

    static int xmp_mknod(const char *path, mode_t mode, dev_t rdev) {
        int res;

        if (S_ISREG(mode)) {
            res = open(path, O_CREAT | O_EXCL | O_WRONLY, mode);
            if (res >= 0)
                res = close(res);
        } else if (S_ISFIFO(mode))
            res = mkfifo(path, mode);
        else
            res = mknod(path, mode, rdev);

        if (res == -1)
            return -errno;

        write_to_log("REPORT", "MKNOD", path);

        return 0;
    }

    static int xmp_symlink(const char *from, const char *to) {
        int res = symlink(from, to);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "SYMLINK", to);

        return 0;
    }

    static int xmp_unlink(const char *path) {
        int res = unlink(path);
        if (res == -1)
            return -errno;

        write_to_log("FLAG", "UNLINK", path);

        return 0;
    }

    static int xmp_rmdir(const char *path) {
        int res = rmdir(path);
        if (res == -1)
            return -errno;

        write_to_log("FLAG", "RMDIR", path);

        return 0;
    }

    static int xmp_rename(const char *from, const char *to) {
        int res = rename(from, to);
        if (res == -1)
            return -errno;

        if (isModule(to)) add_module_directory(to);
        else if (isModule(from) && !isModule(to)) {
            merge_files(to);
            delete_module_directory(from);
        }
        else if(isModule(from) && isModule(to)) {
            delete_module_directory(from);
            add_module_directory(to);
        }

        char* matchingPath = findMatchingPath(to);
        if (matchingPath) {
            if (isFile(to)) {
                modularize_file(to);
            } else if (isDirectory(to)) {
                modularize_directory(to);
            }
        }

        char log_entry[1024];
        snprintf(log_entry, sizeof(log_entry), "%s::%s", from, to);
        write_to_log("REPORT", "RENAME", log_entry);

        return 0;
    }

    static int xmp_link(const char *from, const char *to) {
        int res = link(from, to);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "LINK", to);

        return 0;
    }

    static int xmp_chmod(const char *path, mode_t mode) {
        int res = chmod(path, mode);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "CHMOD", path);

        return 0;
    }

    static int xmp_chown(const char *path, uid_t uid, gid_t gid) {
        int res = lchown(path, uid, gid);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "CHOWN", path);

        return 0;
    }

    static int xmp_truncate(const char *path, off_t size) {
        int res = truncate(path, size);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "TRUNCATE", path);

        return 0;
    }

    static int xmp_open(const char *path, struct fuse_file_info *fi) {
        int res = open(path, fi->flags);
        if (res == -1)
            return -errno;

        close(res);

        write_to_log("REPORT", "OPEN", path);

        return 0;
    }

    static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
        int fd;
        int res;

        (void) fi;
        fd = open(path, O_WRONLY);
        if (fd == -1)
            return -errno;

        res = pwrite(fd, buf, size, offset);
        if (res == -1)
            res = -errno;

        close(fd);

        write_to_log("REPORT", "WRITE", path);

        return res;
    }

    static int xmp_statfs(const char *path, struct statvfs *stbuf) {
        int res = statvfs(path, stbuf);
        if (res == -1)
            return -errno;

        write_to_log("REPORT", "STATFS", path);

        return 0;
    }

    static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
        int res = open(path, fi->flags | O_CREAT, mode);
        if (res == -1)
            return -errno;

        close(res);

        write_to_log("REPORT", "CREATE", path);

        return 0;
    }

    static int xmp_release(const char *path, struct fuse_file_info *fi) {
        (void) path;
        (void) fi;

        write_to_log("REPORT", "RELEASE", path);

        return 0;
    }

    static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi) {
        (void) path;
        (void) isdatasync;
        (void) fi;

        write_to_log("REPORT", "FSYNC", path);

        return 0;
    }

    static void clear_module_directories() {
        FILE* file = fopen("module_directories.txt", "w");
        if (file == NULL) {
            perror("Failed to open module_directories.txt");
            return;
        }
        fclose(file);
    }

    static struct fuse_operations xmp_oper = {
        .getattr = xmp_getattr,
        .access = xmp_access,
        .readdir = xmp_readdir,
        .read = xmp_read,
        .mkdir = xmp_mkdir,
        .mknod = xmp_mknod,
        .symlink = xmp_symlink,
        .unlink = xmp_unlink,
        .rmdir = xmp_rmdir,
        .rename = xmp_rename,
        .link = xmp_link,
        .chmod = xmp_chmod,
        .chown = xmp_chown,
        .truncate = xmp_truncate,
        .open = xmp_open,
        .write = xmp_write,
        .statfs = xmp_statfs,
        .create = xmp_create,
        .release = xmp_release,
        .fsync = xmp_fsync,
    };

    int main(int argc, char *argv[]) {
        umask(0);

        log_file = fopen("/home/kali/fs_module.log", "a");
        if (log_file == NULL) {
            perror("Failed to open log file");
            return -1;
        }
        clear_module_directories();

        int ret = fuse_main(argc, argv, &xmp_oper, NULL);

        fclose(log_file);

        return ret;
    }
    
  Penjelasan kode
  Membuat fs log, membuat log berisi direktori yang berawalan module, setiap ada fungsi system call memanggil fungsi untuk mencatat log. setiap ada file/folder masuk ke direktori modul, dan ada membuat/renmae folder bernama awalan module, lakukan modular. jika folder sudha bukan awalan module lakukan penyatuan  
  __Testing__   
    Before  
  ![before](https://i.ibb.co/8zWPswp/image.png)
    after  
  ![after](https://i.ibb.co/4NQKD8m/image.png)

  __Kendala__  
    Cara membuat agar di fuse  menyatu namun aslinya masih modular

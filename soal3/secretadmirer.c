#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/stat.h>

#ifndef DT_DIR
#define DT_DIR 4
#endif

static const char *dirpath = "/home/kali/inifolderetc/sisop";

// Fungsi rekursif untuk mengunjungi setiap direktori dan subdirektorinya
static int visit_directory(const char *path, void *buf, fuse_fill_dir_t filler)
{
    DIR *dp;
    struct dirent *de;

    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char modified_name[1000];
        memset(modified_name, '\0', sizeof(modified_name));

        strcpy(modified_name, de->d_name);

        // Menambahkan file atau direktori yang telah dimodifikasi ke buffer yang akan diisi
        if (filler(buf, modified_name, &st, 0))
            break;

        // Memanggil secara rekursif subdirektori
        if (de->d_type == DT_DIR && strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
        {
            char new_path[1000];
            sprintf(new_path, "%s/%s", path, de->d_name);
            visit_directory(new_path, buf, filler);
        }
    }

    closedir(dp);
    return 0;
}

// Fungsi untuk mendapatkan atribut (metadata) dari file atau direktori
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

// Fungsi untuk membaca direktori
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    (void)offset;
    (void)fi;

    visit_directory(path, buf, filler);

    return 0;
}

// Fungsi untuk membaca isi dari file
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;

    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};

int main(int argc, char *argv[])
{
    // Menjalankan FUSE
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define maksimal 3000

int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    FILE *progam_file;
    char namafile[] = "FIFA23_official_data.csv";
    progam_file = fopen(namafile, "r");
    if (progam_file == NULL) {
        printf("Tidak bisa membuka file %s\n", namafile);
        exit(1);
    }
    char line[maksimal];
    fgets(line, maksimal, progam_file);
    printf("\n\033[0;34m%-30s \033[0;35m%-10s\033[0m "
           "\033[0;36m%-30s\033[0m "
           "\033[0;33m%-20s\033[0m "
           "\033[0;32m%-11s\033[0m "
           "\033[0;31m%s\033[0m\n",
            "Nama Pemain", "Umur", "Klub", "Negara", "Potensi", "Foto");
    printf("%-30s %-10s %-30s %-20s %-11s %s\n",
            "------------------------------",
            "----------",
            "------------------------------",
            "--------------------",
            "-----------",
            "------------------------------");
    while (fgets(line, maksimal, progam_file)) {
        char *id = strtok(line, ",");
        char *name = strtok(NULL, ",");
        char *age = strtok(NULL, ",");
        char *photo = strtok(NULL, ",");
        char *nationality = strtok(NULL, ",");
        char *flag = strtok(NULL, ",");
        char *overall = strtok(NULL, ",");
        char *potential = strtok(NULL, ",");
        char *club = strtok(NULL, ",");
        char *club_logo = strtok(NULL, ",");
        char *value = strtok(NULL, ",");
        char *wage = strtok(NULL, ",");
        char *special = strtok(NULL, ",");
        char *preferred_foot = strtok(NULL, ",");
        char *international_reputation = strtok(NULL, ",");
        char *weak_foot = strtok(NULL, ",");
        char *skill_moves = strtok(NULL, ",");
        char *work_rate = strtok(NULL, ",");
        char *body_type = strtok(NULL, ",");
        char *real_face = strtok(NULL, ",");
        char *position = strtok(NULL, ",");
        char *joined = strtok(NULL, ",");
        char *loaned_from = strtok(NULL, ",");
        char *contract_valid_until = strtok(NULL, ",");
        char *height = strtok(NULL, ",");
        char *weight = strtok(NULL, ",");
        char *release_clause = strtok(NULL, ",");
        char *kit_number = strtok(NULL, ",");
        char *best_overall_rating = strtok(NULL, ",");
        if (atoi(age) < 25 && atoi(potential) > 85 && strcmp(club, "Manchester City") != 0) {
            printf("\033[0;34m%-30s \033[0;35m%-10s\033[0m "
                   "\033[0;36m%-30s\033[0m "
                   "\033[0;33m%-20s\033[0m "
                   "\033[0;32m%-11s\033[0m "
                   "\033[0;31m%s\033[0m\n",
                    name, age, club, nationality, potential, photo);
        }
    }
    fclose(progam_file);
    return 0;
}
